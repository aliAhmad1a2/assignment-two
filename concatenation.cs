using System.Data.Common;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;


namespace AssignmentTwo;

public class concatenation
{
    
    public static bool checkGrayScaleOrNot(Bitmap bmp)
    {
        if (bmp.PixelFormat == PixelFormat.Format8bppIndexed)
        {
            return true;
        }

        return false;
        
    }
    
    public static void vertical(Bitmap image1,Bitmap image2)
        {
            

            int maxWidth = Math.Max(image1.Width, image2.Width);
            int totalHeight = image1.Height + image2.Height;
            Bitmap outputImage = new Bitmap(maxWidth, totalHeight);

            BitmapData imageData1 = image1.LockBits(new Rectangle(0, 0, image1.Width, image1.Height),
                ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
            BitmapData imageData2 = image2.LockBits(new Rectangle(0, 0, image2.Width, image2.Height),
                ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
            BitmapData outputData = outputImage.LockBits(new Rectangle(0, 0, maxWidth, totalHeight),
                ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            int image1Stride = imageData1.Stride;
            int image2Stride = imageData2.Stride;
            int outputStride = outputData.Stride;

            unsafe
            {
                byte * imageData1Ptr = (byte *)imageData1.Scan0;
                byte * imageData2Ptr = (byte *)imageData2.Scan0;
                byte * outputDataPtr = (byte *)outputData.Scan0;
                int nHeight = image1.Height;
                for (int y = 0; y < nHeight; y++)
                {
                    byte * srcPtr = imageData1Ptr + y * image1Stride;
                    byte * dstPtr = outputDataPtr + y * outputStride;

                    for (int x = 0; x < image1Stride; x++)
                    {
                        dstPtr[x] = srcPtr[x];
                    }
                }

                int image2StartY = image1.Height;
                int nHeight2 = image2.Height;
                for (int y = 0; y <nHeight2 ; y++)
                {
                    byte * srcPtr = imageData2Ptr + y * image2Stride;
                    byte * dstPtr = outputDataPtr + (image2StartY + y) * outputStride;

                    for (int x = 0; x < image2Stride; x++)
                    {
                        dstPtr[x] = srcPtr[x];
                    }
                }
            }

            image1.UnlockBits(imageData1);
            image2.UnlockBits(imageData2);
            outputImage.UnlockBits(outputData);
            outputImage.Save("../../../Images/newVertical.jpg");


        }
    
    
    public static void Horizontal(Bitmap image1, Bitmap image2)
    {

        int totalWidth = image1.Width + image2.Width;
        int maxHeight = Math.Max(image1.Height, image2.Height);
        Bitmap outputImage = new Bitmap(totalWidth, maxHeight);

        BitmapData imageData1 = image1.LockBits(new Rectangle(0, 0, image1.Width, image1.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
        BitmapData imageData2 = image2.LockBits(new Rectangle(0, 0, image2.Width, image2.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
        BitmapData outputData = outputImage.LockBits(new Rectangle(0, 0, totalWidth, maxHeight), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

        int image1Stride = imageData1.Stride;
        int image2Stride = imageData2.Stride;
        int outputStride = outputData.Stride;

        unsafe
        {
            byte* imageData1Ptr = (byte*)imageData1.Scan0;
            byte* imageData2Ptr = (byte*)imageData2.Scan0;
            byte* outputDataPtr = (byte*)outputData.Scan0;
            
            int imHeight = image1.Height;
            int im2Height = image2.Height;
            int imWidth = image1.Width;
            
            for (int y = 0; y < maxHeight; y++)
            {
                byte* dstPtr = outputDataPtr + y * outputStride;

                if (y < imHeight)
                {
                    byte* srcPtr = imageData1Ptr + y * image1Stride;

                    for (int x = 0; x < image1Stride; x++)
                    {
                        dstPtr[x] = srcPtr[x];
                    }
                }

                if (y <im2Height )
                {
                    byte* srcPtr = imageData2Ptr + y * image2Stride;

                    for (int x = 0; x < image2Stride; x++)
                    {
                        dstPtr[(imWidth * 4) + x] = srcPtr[x];
                    }
                }
            }
        }

        image1.UnlockBits(imageData1);
        image2.UnlockBits(imageData2);
        outputImage.UnlockBits(outputData);
        
        outputImage.Save("../../../Images/newHorizantal.jpg");



    }

}